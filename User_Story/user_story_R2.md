User Story: Enhanced Trading Features for BigBucks Platform

**Title:** Implementing Market Orders for Efficient Stock Trading

**As a** BigBucks platform account holder,  
**I want** to have the ability to buy and sell shares of stock using market orders directly from my account,  
**So that** I can manage my investments more actively and efficiently based on market dynamics.

**Acceptance Criteria:**

1. **Market Order Execution:**  
   - **Given** I have sufficient funds in my account,  
   **When** I place a 'buy' market order for a specific number of shares for a stock (e.g., 100 shares of MSFT),  
   **Then** the system should deduct the appropriate amount of cash from my account based on the market price at the time of the transaction and credit the specified number of shares to my portfolio.

2. **Historical Trading Simulation:**  
   - **Given** I am testing trading strategies with historical data,  
   **When** I input a 'buy' or 'sell' market order with specific details (e.g., buy 100 shares of MSFT on 1/20/2021),  
   **Then** the system should use the adjusted close price for the transaction, update my account accordingly, and ensure the transaction reflects in my account's history with an accurate timestamp.

3. **Selling Shares:**  
   - **Given** I own shares of a stock,  
   **When** I place a 'sell' market order for a specific number of shares of that stock (e.g., sell 100 shares of MSFT),  
   **Then** the system should remove the specified number of shares from my portfolio, credit the appropriate amount of cash to my account based on the market price at the time of the transaction, and record the transaction with an accurate timestamp.

4. **Initial Account Funding:**  
   - **Given** I am a new or existing account holder,  
   **When** the new trading features are implemented and I log into my account,  
   **Then** I should see a credit of $1,000,000 in my cash account to be used for buying and selling stocks as part of the platform's trading simulation environment.

5. **User Interface and Experience:**  
   - **Given** I am using the BigBucks platform for trading,  
   **When** I navigate to the trading section,  
   **Then** I should find intuitive options to place both 'buy' and 'sell' orders, view my transaction history, and monitor my current holdings and cash balance with clarity and ease.

**Note:** The system should ensure that all transactions are simulated accurately based on the provided historical data and adjusted for market conditions, without executing real trades. This feature aims to enhance the trading experience on the BigBucks platform, allowing users to practice and refine their investment strategies in a risk-free environment.