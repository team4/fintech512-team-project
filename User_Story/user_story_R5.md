User Story: Investment Holdings Report Enhancement

**Title:** Investment Holdings Report Enhancement by Users

**As a** BigBucks platform user,  
**I want** to easily view and generate detailed reports of my stock holdings,
**So that** I can track the performance, get insights into my investment distribution, and make informed decisions about buying or selling assets.

**Acceptance Criteria:**

1. **Consolidated Holdings View:**  
   - **Given** I am logged into my BigBucks account,
   **When** I click on the 'Investment' tab,
   **Then** I should be presented with an at-a-glance summary of my investments, including ticker symbols, average cost and quantity of shares held.

2. **Historical Value Tracking:**  
   - **Given** I want to understand the historical performance of my portfolio,
   **When** I navigate to the 'Investment' section,
   **Then** I should be able to see a graph displaying the historical value of my entire portfolio over a selectable time frame.

3. **Real-time Price Updates:**  
   - **Given** stock prices constantly fluctuate,
   **When** I view my portfolio in the investment section,
   **Then** the platform should provide real-time price updates for each stock, ensuring that I have the latest information on my holdings.

**Note:** This feature set should provide a holistic and interactive way to monitor investments on the BigBucks platform, equipped with both visualization and detailed data to cater to investors who wish to actively manage their portfolios.

**Title:** Report Enhancement by Administrators

**As a** BigBucks platform administrator,  
**I want** the ability to view and generate aggregated reports of stock holdings and market orders across all user accounts,
**So that** I can oversee the platform's overall investment landscape and ensure that trade executions are proceeding smoothly and accurately.

**Acceptance Criteria:**

1. **Aggregated User Holdings Overview:**  
   - **Given** I am analyzing the investment spread across all accounts,
   **When** I access the 'All Users' section,
   **Then** I should be able to see a consolidated list of all holdings, displaying each user's stock holdings by ticker symbol, average cost, quantity, and current price per share.

2. **Market Order Summary:**  
   - **Given** I need to monitor trading activity on the platform,
   **When** I access 'View Orders',
   **Then** the system should provide me with a summary of that time range’s market orders across all users, detailing the ticker symbol, total shares bought, total shares sold, and aggregate volume for each stock. And I can also select a specific User ID.

3. **Cross-User Portfolio Comparisons:**  
   - **Given** the need for comparative analysis across user portfolios,
   **When** I select multiple user IDs for comparison,
   **Then** the system should provide specific comparisons of selected portfolios
   Cross-User Portfolio Comparisons.

**Note:** These features are essential for administrators to manage the BigBucks platform effectively. They provide the necessary oversight tools to ensure a secure, compliant, and operationally efficient trading environment. The aggregated data will also support administrators in identifying platform usage patterns and making data-driven decisions to improve user experience.