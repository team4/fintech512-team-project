## Team 4 - Schedule.md

Meet TA Rebecca Xiao (qx42) every Saturday morning 10 am. 

| Status | Date       |
|--------|------------|
| √      | 2024-03-02 |
|        | 2024-03-22 |
|        | 2024-03-29 |
|        | 2024-04-05 |