## Team 4 Project - Requirements

###  Requirements 1 - Website Interaction

`Requirements #`: major-01-f	    `Type`: functional	`Event/Use Case`:

`Description`: Enable customers to interact with your website

`Rationale`: Basic function requirements which allows users to access other functions

`Origination`: Term Project Major Requirements 01 part 1

`Fit Criterion`: 
1. Website should detect user behavior and make changes according to the behavior. 

`Customer Satisfaction`: 4	`Customer Dissatisfaction`: 5	`Conflicts`: N/A

`Priority`: 5

`Supporting Materials`:

`History`:

###  Requirements 2 - Account

`Requirements #`: major-01-02	`Type`: functional	`Event/Use Case`:

`Description`: Enable customers to create accounts

`Rationale`: Allows users to create their account for trading activities

`Origination`: Term Project Major Requirements 01 part 2

`Fit Criterion`:
1. After user creates an account, there should be a new account stored in the server.

`Customer Satisfaction`: 3	`Customer Dissatisfaction`: 4	`Conflicts`: N/A

`Priority`: 5

`Supporting Materials`:

`History`:

### Requirements 3 - Trading

`Requirements #`: major-02-01	`Type`: functional	`Event/Use Case`:

`Description`: Enable account holders to ‘buy’ and ‘sell’ shares of stock

`Rationale`: Basic function requirements that fits the purpose of the product

`Origination`: Term Project Major Requirements 02 part 1

`Fit Criterion`:
1.	After buying activity, shares of stock should be added to the account, and balance should be subtracted. 
2.	After selling activity, shares of stock should be subtracted from the account, and balance should be added. 
3.	If users do not have enough cash, the purchase activity will be suspended, and warnings will appear.
4.	If users do not have enough shares of that stock, the selling activity will be suspended, and warnings will appear.

`Customer Satisfaction`: 5	`Customer Dissatisfaction`: 3	`Conflicts`: N/A

`Priority`: 5

`Supporting Materials`:

`History`:

### Requirements 4 - History

`Requirements #`: major-02-02	`Type`: functional	`Event/Use Case`:

`Description`: Enable user to view historical data

`Rationale`: Users could view their historical data to track trading activities, which could be used for tax report. 

`Origination`: Term Project Major Requirements 02

`Fit Criterion`: 
1.	When user finishes a trade, that activity should be added to the historical database.
2.	If user cancel the trade, the activity should not be added to the historical database.

`Customer Satisfaction`: 3	`Customer Dissatisfaction`: 4	`Conflicts`:

`Priority`: 4

`Supporting Materials`:

`History`:

### Requirements 5 - Performance Measurement

`Requirements #`: major-04-01	`Type`: functional	`Event/Use Case`:

`Description`: Enable users to view efficient frontier of their portfolio

`Rationale`: Enhance user’s risk awareness and help their decision process

`Origination`: Term Project Major Requirements 04

`Fit Criterion`:
1.	Test whether the Sharpe ratio is correct.
2.	Test whether the efficient frontier’s list of values is correct.

`Customer Satisfaction`: 4	Customer Dissatisfaction: 3	Conflicts: N/A

`Priority`: 3

`Supporting Materials`:

`History`: