## Sprint 3 Plan

Team 4

### 1. Initialize the VM server

We want to make sure:

1. Everyone in the team can access the server.
2. The BigBucks website could function on the server. 

### 2. Finalize the Requirement

As we delve into the project description, we will come up with a finalized version of Requirement, which will serve as the guidance for our project. We may redesign the UML diagram by applying the **C4 model** (context - container - component - code) for better abstraction.

### 3. Create a Working Repository

Our team will create a project code repository for the team to work on. 

### 4. Front-end Page

We will create several blank pages to mimic the user experience. 

### 5. Back-end Implementation and Testing 

Based on the design diagrams, our team will implement some back-end functionalities, and write unit test files for each individual part. 
