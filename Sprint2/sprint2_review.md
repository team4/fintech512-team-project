## Sprint 2 Review

Team 4

### 1. Meeting with TA

Our team had a meeting with TA, during which we reported our weekly progress, rescheduled meeting time, and answered TA's questions. 

### 2. Update UML Diagram

This week, we updated the **UML class diagram** based on an updated requirements table (we took inspiration from the Project Grading Template). 

### 3. Build Server

Our team reserved a Virtual Mechine on Duke Server website, and finished the setup process. 

### 4. GitLab Issue

Our team got used to the GitLab Issue page, and this week's backlogs were written in the form of GitLab Issue with the tag **Product Backlog**.

### 5. Code Testing

We tested our own BigBucks page based on Week 6 assignment. However, a lot of things needs to be changed, as we need to add many requirements to the existing project (next week).
