## Sprint 1 Retrospective Review

Team 4

### 1. Team Formation

During the week before spring break, we finished team formation, and created a GitLab repository for the BigBucks team project, giving access to team members and reporters (Instructors and TAs). This week we need to **set up VCM** for team members. 

### 2. Schedule Setup

We initally set up the weekly meeting time with our TA Rebecca Xiao (qx42) at 10am every Saturday. However, our team want to **discuss with TA about rescheduling**. 

### 3. Requirement Review

We reviewed the requirements table and UML diagram to agree on a version to be submitted for sprint 1. This week, we have a better understanding of the project. We will **update the requirement table and UML diagram** (UML is required for sprint 2).

### 4. Product Backlog

Based on the above work, our team created a 'Product Backlog'. Last week, we created a single .md file as backlog. This week, we are going to **create a backlog in the form of GitLab issues** tagged 'Product Backlog'. 
