## Sprint 6 Review 1

Team 4

### 1. Meeting with TA

Our group had the final meeting with TA. 

### 2. Finish all project requirements

Our group finished all project requirements, closed the corresponding GitLab issues, and upload the content to GitLab. 

### 3. VCM Server and website

We created a VCM server with address http://team4.colab.duke.edu:5000/

### 4. Video Presentation

Our group recorded the 3 videos demonstrating the functionalities, additional features, and UI, UX design principle. 