## Sprint 5 Retrospective Review

Team 4

### 1. Meeting with TA
Following the progress of previous sprint, our team demonstrated features like account page, administrator page and efficient frontier to TA. We have changed our presentation format so that every team member got the opportunity to talk about their contribution to the project. Furthermore, we discussed with TA about the project topic assignment. 

### 2. Addition of User Story
We analyzed the user's requirement in different scenarios, and wrote the user story to serve as a guideline for our function development.

### 3. Addition of Test Cases
We have added test cases to enhance the reliability and performance of our application. We upload the content to the test_case folder. Next week we are going to split the test cases based on requirements.

### 4. Prototype
We have presented the paper prototype, and our design was approved by TA. Therefore, our team are going to migrate the prototype from paper to Balsamiq wireframe.

### 5. Code Review
We reviewed our code written for requirement 1-5 based on SLOC metric and SOLID principle.