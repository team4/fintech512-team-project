import numpy as np
import pandas as pd
from scipy.optimize import minimize
import statsmodels.api as sm

import requests
from bs4 import BeautifulSoup
import datetime
from scipy.stats import norm

from .stock_av import get_price_series
from .stock_db import generate_plot_data

def analyze_risk(ptfl):
    
    symbols = ptfl['symbol']
    quantities = ptfl['quantity']
    # get 5-year historical data for these
    if len(symbols) != 0:
        past_prices = {}
        for symbol in symbols:
            asset_prices = get_price_series(symbol)
            data = generate_plot_data(symbol, asset_prices)
            past_prices[symbol] = data['adj_close']

        EF_data = efficient_frontier(past_prices)
        
        # sharpe ratio
        weight = []
        total_weight = 0
        for i in range(len(symbols)):
            symbol = symbols[i]
            quantity = quantities[i]
            latest_price = past_prices[symbol][-1]
            weight.append(latest_price * quantity)
            latest_price = float(latest_price)
            quantity = int(quantity)
            total_weight += latest_price * quantity
            
        real_weight = [w / total_weight for w in weight]
        sharpe_data = sharpe_ratio(past_prices, real_weight)
        VaR_data = VaR_normal(past_prices, real_weight)
        
        data = {
            "EF": EF_data,
            "sharpe": sharpe_data,
            "prices": past_prices,
            "VaR": VaR_data
        }
        return data
    else:
        message = "The portfolio does not contain any asset."
        return message
    

def calculate_returns(past_prices):
    
    # align the length of past_prices dict
    min_length = min(len(prices) for prices in past_prices.values())
    adjusted_past_prices = {symbol: prices[-min_length:] for symbol, prices in past_prices.items()}
    
    price_matrix = pd.DataFrame()
    for symbol in adjusted_past_prices.keys():
        price_matrix[symbol] = adjusted_past_prices[symbol]

    returns = price_matrix.pct_change().dropna()
    return returns

# Efficient frontier
# parameter:
#   past_prices =
#   {
#       "Asset1": [price1, price2, ...],
#       "Asset2": [price1, price2, ...],
#       ...
#   }
# returns:
#   {
#       "returns": [return1, return2, ...],
#       "stdev": [sigma1, sigma2, ...]
#   }
def efficient_frontier(past_prices):
    
    returns = calculate_returns(past_prices)
    num_assets = returns.shape[1]
    mean_returns = returns.mean()
    cov_matrix = returns.cov()
        
    # target function
    def ptfl_variance(weights, cov_matrix):
        return np.sqrt(np.dot(weights.T, np.dot(cov_matrix, weights)))
    
    # def portfolio_return(weights):

    #     if abs(np.dot(weights.T, mean_returns) - target_return) < 1e-8:
    #         return 0
        
    #     return np.dot(weights.T, mean_returns) - target_return
    
    def sum_weight(x):
        if abs(np.sum(x) - 1) < 1e-8:
            return 0
        
        return np.sum(x) - 1
    
    bounds = tuple((0,1) for _ in range(num_assets))
    
    # define initial weight
    initial_weights = np.ones(num_assets) / num_assets
    
    # target returns
    target_returns = np.linspace(min(mean_returns), max(mean_returns), 100)
    
    # calculate efficient weight
    efficient_ptfl = []
    for target_return in target_returns:
        def portfolio_return(x):
            return np.dot(x.T, mean_returns) - target_return
            
        constraints = ({'type': 'eq', 'fun': portfolio_return},
                   {'type': 'eq', 'fun': sum_weight})
        result = minimize(ptfl_variance, initial_weights, args=(cov_matrix,), bounds=bounds, method='SLSQP', constraints=constraints)
        efficient_ptfl.append(result.x)

    # returns = [np.dot(weight.T, mean_returns) for weight in efficient_ptfl]
    returns = list(target_returns)
    # print(returns)
    # print(min(returns))
    stdev = [ptfl_variance(weight, cov_matrix) for weight in efficient_ptfl]
    # print(stdev)
    # print(min(stdev))
    
    weights = []
    
    for x in efficient_ptfl:
        weight = list(x)
        weights.append(weight)
    
    return {
        "weight": weights,
        "return": returns,
        "stdev": stdev
    }


# 10-year treasury risk free
# return: <float> ten year treasury yield
def ten_year_treasury():
    today = datetime.date.today()
    year = today.year
    url = "https://home.treasury.gov/resource-center/data-chart-center/interest-rates/TextView?type=daily_treasury_yield_curve&field_tdr_date_value=" + str(year)
    html = requests.get(url)
    soup = BeautifulSoup(html.text, "lxml")
    info = soup.select("#block-hamilton-content > div > div > div.view-content > table > tbody > tr:nth-last-child(1) > td.views-field.views-field-field-bc-10year")
    return float(info[0].get_text().strip()) / 100 / 252

# Sharpe ratio
def sharpe_ratio(past_prices, weight):
    rf = ten_year_treasury()
    returns = calculate_returns(past_prices)
    mean_returns = returns.mean()
    cov_matrix = returns.cov()
    
    weight = np.array(weight)
    rp = np.dot(weight.T, mean_returns)
    sigma = np.sqrt(np.dot(weight.T, np.dot(cov_matrix, weight)))
    
    sharpe_ratio = (rp - rf)/sigma
    return {
        "weight": weight.tolist(),
        "sharpe_ratio": round(sharpe_ratio, 4),
        "r_p": round(rp * 100, 4),
        "sigma_p": round(sigma * 100, 4) 
    }
    
# calculate price return
def calculate_price_diff(past_prices):
    # align the length of past_prices dict
    min_length = min(len(prices) for prices in past_prices.values())
    adjusted_past_prices = {symbol: prices[-min_length:] for symbol, prices in past_prices.items()}
    
    price_matrix = pd.DataFrame()
    for symbol in adjusted_past_prices.keys():
        price_matrix[symbol] = adjusted_past_prices[symbol]

    returns = price_matrix.diff().dropna()
    return returns
    
# calculate regression
def calculate_alpha_beta(stock_returns, spy_returns, return_length):
    spy_returns = spy_returns.tail(return_length)
    stock_returns = stock_returns.reset_index(drop=True)
    spy_returns = spy_returns.reset_index(drop=True)

    # add a constant for intercept
    spy_returns = sm.add_constant(spy_returns)

    # create a linear regression model
    model = sm.OLS(stock_returns, spy_returns)
    results = model.fit()

    alpha = results.params['const']
    beta = results.params['SPY']

    return alpha, beta

# VaR
def VaR_normal(past_prices, weight):
    returns = calculate_returns(past_prices)
    mean_returns = returns.mean()
    cov_matrix = returns.cov()
    
    weight = np.array(weight)
    rp = np.dot(weight.T, mean_returns)
    sigma = np.sqrt(np.dot(weight.T, np.dot(cov_matrix, weight)))
    
    VaR_df = VaR_norm_df(0.05, rp, sigma)
    return VaR_df

# helper function return VaR normal df
def VaR_norm_df(alpha, mu, sigma):
    VaR = -norm.ppf(alpha, loc = mu, scale = sigma)
    diff = -norm.ppf(alpha, loc = 0, scale = sigma)
    return {"VaR Absolute": round(VaR, 4), 
            "VaR Diff": round(diff, 4)}