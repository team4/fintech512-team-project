DROP TABLE IF EXISTS user;
DROP TABLE IF EXISTS stock_data;
DROP TABLE IF EXISTS orders;
DROP TABLE IF EXISTS holding;
DROP TABLE IF EXISTS watchlist;

CREATE TABLE user (
  id                INTEGER PRIMARY KEY AUTOINCREMENT,
  username          TEXT UNIQUE NOT NULL,
  password          TEXT NOT NULL,
  balance           DECIMAL(16,6) NOT NULL
);

INSERT INTO user (
  username, password, balance
)
VALUES (
  '__admin__', 'pbkdf2:sha256:260000$lEsSGRIZhQ6G6hZy$a4247fb70c2cae90fd221cf78f62b62e4436cae7bd63751d0585969a3a729267', 1000000
);

CREATE TABLE stock_data (
  stock_symbol      VARCHAR(5)    NOT NULL,  
  closing_date      DATE          NOT NULL,
  open_price        DECIMAL(16,6) NOT NULL,
  high_price        DECIMAL(16,6) NOT NULL,
  low_price         DECIMAL(16,6) NOT NULL,
  close_price       DECIMAL(16,6) NOT NULL,
  adj_close_price   DECIMAL(16,6) NOT NULL,
  volume            BIGINT        NOT NULL,
  PRIMARY KEY (stock_symbol, closing_date)
);

CREATE TABLE orders (
  id                INTEGER PRIMARY KEY AUTOINCREMENT,
  user_id           INTEGER NOT NULL,
  order_type        VARCHAR(5) NOT NULL,
  stock_symbol      VARCHAR(5) NOT NULL,
  order_time        DATE NOT NULL,
  unit_price        DECIMAL(16,6) NOT NULL,
  quantity          INTEGER NOT NULL,
  volume            DECIMAL(16,6) NOT NULL,
  FOREIGN KEY (user_id) REFERENCES user(id)
);

CREATE TABLE holding (
  id                INTEGER PRIMARY KEY AUTOINCREMENT,
  user_id           INTEGER NOT NULL,
  stock_symbol      VARCHAR(5) NOT NULL,
  average_price     DECIMAL(16,6) NOT NULL,
  quantity          INTEGER NOT NULL,
  volume            DECIMAL(16,6) NOT NULL,
  FOREIGN KEY (user_id) REFERENCES user(id)
);

CREATE TABLE watchlist (
  user_id           INTEGER NOT NULL,
  stock_symbol      VARCHAR(5) NOT NULL,
  FOREIGN KEY (user_id) REFERENCES user(id)
);
