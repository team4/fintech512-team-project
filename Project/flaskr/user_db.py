from datetime import date as d

import pandas as pd
from .db import get_db
from .stock_info import update_stock_data

# get user's current value
def get_user_cash(user_id):
    db = get_db()
    user_cash = db.execute(
        'SELECT * FROM user WHERE id = ?',
        (user_id,),
    ).fetchone()
    return user_cash['balance']


def get_latest_price_in_ptfl(symbols):
    db = get_db()
    prices = []
    for symbol in symbols:
        price = db.execute(
            'SELECT adj_close_price FROM stock_data WHERE stock_symbol = ? '
            ' ORDER BY closing_date DESC LIMIT 1',
            (symbol,),
        ).fetchone()
        price = price['adj_close_price']
        prices.append(price)
        
    return prices
    
        


# get the user's trade history
def get_user_orders(user_id):
    db = get_db()
    user_orders = db.execute(
        'SELECT * FROM orders WHERE user_id = ? ORDER BY order_time',
        (user_id,)
    ).fetchall()
    db.commit()
    
    id = []
    type = []
    symbol = []
    time = []
    price = []
    quantity = []
    volume = []
    
    if len(user_orders) != 0:
        for order in user_orders:
            id.append(order['id'])
            type.append(order['order_type'])
            symbol.append(order['stock_symbol'])
            time.append(order['order_time'])
            price.append(order['unit_price'])
            quantity.append(order['quantity'])
            volume.append(order['volume'])
            
    for i in range(len(volume)):
        volume[i] = round(volume[i], 2)
        
    
    order_data = {
        'id': id,
        'type': type,
        'symbol': symbol,
        'time': time,
        'price': price,
        'quantity': quantity,
        'volume': volume
    }
    return order_data

# get the user's holding
def get_user_portfolio(user_id):
    db = get_db()
    user_portfolio = db.execute(
        'SELECT * FROM holding WHERE user_id = ? ORDER BY volume DESC',
        (user_id,)
    ).fetchall()
    db.commit()       
    
    symbol = []
    average_cost = []
    quantity = []
         
    if len(user_portfolio) != 0:
        for asset in user_portfolio:
            symbol.append(asset['stock_symbol'])
            average_cost.append(asset['average_price'])
            quantity.append(asset['quantity'])
            
    for i in range(len(average_cost)):
        average_cost[i] = round(average_cost[i], 2)
            
    portfolio_data = {
        'symbol': symbol,
        'average_cost': average_cost,
        'quantity': quantity,
    }
    
    return portfolio_data


# get the user's investing chart
def get_historical_portfolio(user_id):
    
    # get unique all symbols traded
    db = get_db()
    all_symbols_traded = db.execute(
        'SELECT DISTINCT stock_symbol FROM orders WHERE user_id = ?',
        (user_id,),
    ).fetchall()
    
    # if user have trading history
    if len(all_symbols_traded) != 0:
        
        user_order = get_user_orders(user_id) 
        dates = user_order["time"]
        start_date = min(dates)
        end_date = d.today()
        
        # define a dataframe to hold user portfolio for each stock by date
        date_range = pd.date_range(start=start_date, end=end_date)
        df = pd.DataFrame(index=date_range)
        
        # for each stock in the stock list
        for symbol in all_symbols_traded:
            this_symbol = symbol['stock_symbol']
            # check if stock db is current
            # if not, update stock db
            db = get_db()
            db_latest_date = db.execute(
                'SELECT closing_date FROM stock_data '
                ' WHERE stock_symbol = ? '
                ' ORDER BY closing_date DESC',
                (this_symbol,),
            ).fetchone()
            
            if db_latest_date['closing_date'] != end_date:
                update_stock_data(this_symbol)
            
            # a dict with stock prices from start_date to today
            # key: stock symbol
            # value: a vector of prices from old to new
            prices_data = db.execute(
                'SELECT closing_date, adj_close_price FROM stock_data '
                'WHERE stock_symbol = ? '
                'AND closing_date BETWEEN ? AND ? ORDER BY closing_date',
                (this_symbol, start_date, end_date,),
            ).fetchall()

            user_stock_quantity = 0
            for price_data in prices_data:
                date = price_data['closing_date']
                price = price_data['adj_close_price']
                # see if user trade stock on this date
                user_trades= db.execute(
                    'SELECT order_type, quantity FROM orders '
                    'WHERE order_time = ? AND user_id = ?',
                    (date, user_id,),
                ).fetchall()
                
                # some trade happened
                if len(user_trades) != 0:
                    total_quantity_change = 0
                    # add buy and subtract sell
                    for user_trade in user_trades:
                        type = user_trade['order_type']
                        if type == 'SELL':
                            total_quantity_change = total_quantity_change - user_trade['quantity']
                        else:
                            total_quantity_change = total_quantity_change + user_trade['quantity']
                    # update user's stock share on that date
                    user_stock_quantity = user_stock_quantity + total_quantity_change
                # for each symbol and each date, add user's value
                df.loc[date, this_symbol] = price * user_stock_quantity
        
        df = df.dropna()
        value_by_date = df.sum(axis=1)
        value_list = value_by_date.tolist()
        date_list = df.index.tolist()      
        result = {
            'message': 'Generated historical portfolio.',
            'date': date_list,
            'value': value_list
        }
        return result
                
    else:
        message = "This user has not traded."
        return {'message': message}


def add_to_watchlist(user_id, symbol):
    db = get_db()
    stock = db.execute(
        'SELECT * FROM watchlist WHERE user_id = ? AND stock_symbol = ?',
        (user_id, symbol,),
    ).fetchone()
    if stock is None:
        print("Added ", symbol, " to watchlist")
        db.execute(
            'INSERT INTO watchlist (user_id, stock_symbol) VALUES (?, ?)',
            (user_id, symbol,),
        )
        db.commit()
    else:
        print(symbol, " is already in watchlist")
    
    
def remove_from_watchlist(user_id, symbol):
    db = get_db()
    db.execute(
        'DELETE FROM watchlist WHERE user_id = ? AND stock_symbol = ?',
        (user_id, symbol,),
    )
    db.commit()
    print("Removed ", symbol, " from watchlist")
    
    
def get_watchlist(user_id):
    
    # get watchlist
    db = get_db()
    watchlist = db.execute(
        'SELECT stock_symbol FROM watchlist WHERE user_id = ?',
        (user_id,),
    ).fetchall()
    if len(watchlist) != 0:
        symbols = []
        prices = []
        # get latest adj closed price
        for item in watchlist:
            symbol = item['stock_symbol']
            price = db.execute(
                'SELECT adj_close_price FROM stock_data WHERE stock_symbol = ? ORDER BY closing_date DESC LIMIT 1',
                (symbol,),
            ).fetchone()
            stock_price = price['adj_close_price']
            symbols.append(symbol)
            prices.append(stock_price)
            
        dict = {
            'message': None,
            'symbol': symbols,
            'price': prices
        }
        return dict
    else:
        message = "The user has not added anything to the watchlist."
        return {'message': message}
    