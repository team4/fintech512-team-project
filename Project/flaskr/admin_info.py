from flask import (
    Blueprint, Flask, flash, g, redirect, render_template, request, session, url_for, jsonify, make_response
)

from datetime import datetime, date

from .auth import login_required
from .ptfl_analysis import analyze_risk, VaR_normal
from .admin_db import (
    get_all_user_portfolio, get_orders_by_date, get_total_portfolio
)

from werkzeug.exceptions import abort

bp = Blueprint('admin_info', __name__)

@bp.route('/dashboard')
@login_required
def dashboard():
    
    return render_template('admin_info/admin_dashboard.html')

# Bigbucks R4: analyze overall risk-return profile
@bp.route('/total_risk')
@login_required
def total_risk():
    ptfl = get_total_portfolio()
    symbols = ptfl['symbol']
    
    risk_data = analyze_risk(ptfl)
    
    if risk_data == "The portfolio does not contain any asset.":
        return render_template('admin_info/total_risk.html',
                                empty_ptfl=True)
    else:
        VaR_data = risk_data["VaR"]
        sharpe_data = risk_data["sharpe"]
        EF_data = risk_data["EF"]
        return render_template('admin_info/total_risk.html',
                            empty_ptfl=False,
                            symbols=symbols,
                            sharpe_data=sharpe_data,
                            EF_data=EF_data,
                            VaR_data=VaR_data)

# Bigbucks R5: List stocks across all users by ticker symbol, name, shares held, and price per share
@bp.route('/view_users')
@login_required
def view_users():
    ptfl_data = get_all_user_portfolio()
    row_num = len(ptfl_data['user_id'])
    
    unique_user_id = sorted(set(ptfl_data['user_id']))

    return render_template('admin_info/user_portfolio.html', 
                           unique_user_id=unique_user_id,
                           row_num=row_num,
                           keys=ptfl_data.keys(),
                           ptfl_data=ptfl_data)

# Bigbucks R5: List summary of current day's market orders across all users by ticker symbol, name, shares bought and shares sold.
@bp.route('/view_orders', methods=('GET', 'POST'))
@login_required
def view_orders():
    if request.method == 'POST':
        error = None
        start_date_str = request.form['start-date']
        end_date_str = request.form['end-date']
        
        # convert string to date
        start_date = datetime.strptime(start_date_str, '%Y-%m-%d').date()
        end_date = datetime.strptime(end_date_str, '%Y-%m-%d').date()
        
        if start_date > end_date:
            error = "Cannot select end date prior to start date"
            
        if error is not None:
            flash(error)
            return redirect(url_for('admin_info.dashboard'))
        else:
            order_data = get_orders_by_date(start_date, end_date)
            row_num = len(order_data['id'])
            
            unique_user_id = sorted(set(order_data['user_id']))
            
            return render_template('admin_info/market_orders.html',
                                   unique_user_id=unique_user_id,
                                   row_num=row_num,
                                   keys=order_data.keys(),
                                   order_data=order_data,
                                   start_date=start_date,
                                   end_date=end_date)
        