function makePlot(data, ma_data, symbol, index) {
    var chartData = [{
        x: data["date"].slice(index),
        close: data["close"].slice(index),
        decreasing: {
            line: {color: 'rgba(160, 160, 160, 0.8)', width: 0.8},
            fillcolor: 'red'
        }, // Customization for decreasing candles
        high: data["high"].slice(index),
        increasing: {
            line: {color: 'rgba(160, 160, 160, 0.8)', width: 0.8},
            fillcolor: 'green'
        }, // Customization for increasing candles
        low: data["low"].slice(index),
        open: data["open"].slice(index),
        type: 'candlestick',
        name: 'Price',
        xaxis: 'x',
        yaxis: 'y'
    }];

    // add MA10
    chartData.push({
        x: ma_data['date'].slice(index),
        y: ma_data['MA10'].slice(index),
        type: 'scatter',
        mode: 'lines',
        name: 'MA10',
        line: {color: 'rgb(0, 204, 204, 0.8)'}
    });

    // add MA50
    chartData.push({
        x: ma_data['date'].slice(index),
        y: ma_data['MA50'].slice(index),
        type: 'scatter',
        mode: 'lines',
        name: 'MA50',
        line: {color: 'rgb(220, 120, 180, 0.8)'}
    });

    // add MA200
    chartData.push({
        x: ma_data['date'].slice(index),
        y: ma_data['MA200'].slice(index),
        type: 'scatter',
        mode: 'lines',
        name: 'MA200',
        line: {color: 'rgb(255, 178, 102, 0.8)'}
    });

    var layout = {
        title: 'Stock Prices for ' + symbol,
        xaxis: {
            title: 'Date'
        },
        yaxis: {
            title: 'Price'
        }
    };

    Plotly.newPlot('candlestickGraph', chartData, layout);
}

function efficient_frontier(expectedReturns, standardDeviations, userStd, userReturn, symbols, weights, userWeights) {

    var frontierTrace = {
        x: standardDeviations.map(value => value * 100),
        y: expectedReturns.map(value => value * 100),
        mode: 'lines+markers',
        name: 'Efficient Frontier',
        marker: {
            color: 'rgb(80, 204, 204, 0.8)'
        }
    };

    var userPositionTrace = {
        x: [userStd],
        y: [userReturn],
        mode: 'markers',
        name: 'User Portfolio',
        marker: {
            color: 'rgb(255, 178, 102, 0.8)',
            symbol: 'diamond'
        }
    };

    var layout = {
        xaxis: {
            title: 'Standard Deviation (Risk) (%)',
            range: [0, Math.max(...standardDeviations) * 110]
        },
        yaxis: {
            title: 'Expected Return (%)',
            range: [Math.min(0, (Math.min(...expectedReturns) * 110)), Math.max(...expectedReturns) * 110]
        },
        shapes: [{
            type: 'line',
            x0: userStd,
            y0: 0,
            x1: userStd,
            y1: Math.max(...expectedReturns) * 110,
            line: {
                color: 'rgb(220, 120, 180, 0.8)', 
                dash: 'dot' 
            }
        }]
    };

    var data = [frontierTrace, userPositionTrace];

    Plotly.newPlot('efficientFrontier', data, layout);
}

// Assuming plot_data is available in the global scope or fetched dynamically
// document.addEventListener('DOMContentLoaded', function() {
//     makePlot(plot_data, symbol)
//     makePlot2(plot_data, symbol);
//     efficient_frontier(expectedReturns, standardDeviations, userStd, userReturn, symbols, weights, userWeights)
// });


function hist_ptfl(date, value) {

    var chartData = [{
        x: date,
        y: value,
        type: 'scatter',
        mode: 'lines+markers',
        marker: {
            color: 'orange'
        }
    }];

    var layout = {
        title: 'Historical Portfolio',
        xaxis: {
            title: 'Date'
        },
        yaxis: {
            title: 'Value'
        },
    };

    Plotly.newPlot('historicalPtfl', chartData, layout);
}

function ptfl_pie_chart(symbol, quantity, diversity) {
    var quantity = JSON.parse(quantity);
    var diversity = JSON.parse(diversity);

    var data = [{
        values: diversity,  
        labels: symbol,    
        type: 'pie',
        hoverinfo: 'text',  
        text: symbol.map((s, i) => `${s}: ${quantity[i]} shares`), 
        textinfo: 'none'
      }];
      
    var layout = {
        title: 'Your Portfolio Diversity',
        height: 450,
        width: 550
    };
    
    Plotly.newPlot('pieChart', data, layout);
}

function plot7_1(symbol, stock_date, stock_diff, spy_diff, index) {
    var stockData = {
        x: stock_date.slice(index),
        y: stock_diff.slice(index),
        type: 'scatter',
        mode: 'lines',
        name: symbol,
        marker: {
            color: 'rgb(220, 120, 180, 0.8)'
        }
    };

    var spyData = {
        x: stock_date.slice(index),
        y: spy_diff.slice(index),
        type: 'scatter',
        mode: 'lines',
        name: "SPY",
        marker: {
            color: 'rgb(0, 204, 204, 0.8)'
        }
    };

    var layout = {
        title: 'Daily Price Change of ' + symbol + ' and S&P500',
        xaxis: {
            title: 'Date'
        },
        yaxis: {
            title: 'Daily Price Change ($)'
        }
    };

    var data = [spyData, stockData];
    Plotly.newPlot('graph7_1', data, layout);
}

function plot7_2(symbol, stock_date, stock_returns, spy_returns, index) {
    var stockData = {
        x: stock_date.slice(index),
        y: stock_returns.slice(index),
        type: 'scatter',
        mode: 'lines',
        name: symbol,
        marker: {
            color: 'rgb(220, 120, 180, 0.8)'
        }
    };

    var spyData = {
        x: stock_date.slice(index),
        y: spy_returns.slice(index),
        type: 'scatter',
        mode: 'lines',
        name: "SPY",
        marker: {
            color: 'rgb(0, 204, 204, 0.8)'
        }
    };

    var layout = {
        title: 'Daily Return of ' + symbol + ' and S&P500',
        xaxis: {
            title: 'Date'
        },
        yaxis: {
            title: 'Daily Return'
        }
    };

    var data = [spyData, stockData];
    Plotly.newPlot('graph7_2', data, layout);
}

function plot7_3(symbol, stock_returns, spy_returns, alpha, beta) {
    var scatterData = {
        x: spy_returns,
        y: stock_returns,
        type: 'scatter',
        mode: 'markers',
        name: '(SPY, ' + symbol + ')',
        marker: {
            color: 'rgb(255, 178, 102, 0.8)',
            symbol: 'diamond'
        }
    };

    var regX = [Math.min(...stock_returns), Math.max(...stock_returns)];
    var regY = regX.map(x => alpha + beta * x);

    var lineData = {
        x: regX,
        y: regY,
        type: 'scatter',
        mode: 'lines',
        name: 'Regression Line',
        marker: {
            color: 'rgb(220, 120, 180, 0.8)'
        }
    };

    var layout = {
        title: symbol + ' = ' + alpha.toFixed(4) + ' + ' + beta.toFixed(4) + ' * S&P500',
        xaxis: {
            title: 'SPY'
        },
        yaxis: {
            title: symbol
        }
    };

    var data = [scatterData, lineData];
    Plotly.newPlot('graph7_3', data, layout);
}

function plot14_1(symbol, stock_data, index) {
    var data = [{
        x: stock_data['date'].slice(index),
        y: stock_data['adj_close'].slice(index),
        type: 'scatter',
        mode: 'lines',
        name: symbol,
        marker: {
            color: 'rgb(0, 204, 204, 0.8)'
        }
    }];

    var layout = {
        title: symbol + ' Adjusted Closed Price',
        xaxis: {
            title: 'Date'
        },
        yaxis: {
            title: 'Price'
        }
    };

    Plotly.newPlot('graph14_1', data, layout);
}

function plot14_2(symbol, stock_data, stock_returns, index) {
    var data = [{
        x: stock_data['date'].slice(index),
        y: stock_returns.slice(index),
        type: 'scatter',
        mode: 'lines',
        name: symbol,
        marker: {
            color: 'rgb(0, 204, 204, 0.8)'
        }
    }];

    var layout = {
        title: 'Daily Return of ' + symbol,
        xaxis: {
            title: 'Date'
        },
        yaxis: {
            title: 'Return'
        }
    };

    Plotly.newPlot('graph14_2', data, layout);
}

function plot14_3(symbol, stock_returns, index) {
    var returns = stock_returns.slice(index);

    var prior_returns = returns.slice(0, -1);
    returns = returns.slice(1);

    var data = [{
        x: prior_returns,
        y: returns,
        type: 'scatter',
        mode: 'markers',
        marker: {
            color: 'rgb(255, 178, 102, 0.8)',
            symbol: 'diamond'
        }
    }];

    var layout = {
        title: 'Autocorrelated Return of ' + symbol,
        xaxis: {
            title: 'Return (-1)'
        },
        yaxis: {
            title: 'Return'
        }
    };

    Plotly.newPlot('graph14_3', data, layout);
}

function plot14_4(symbol, stock_returns, index) {
    var data = [{
        x: stock_returns.slice(index),
        type: 'histogram',
        marker: {
            color: 'rgb(220, 120, 180, 0.8)'
        }
    }];

    var layout = {
        title: 'Histogram of Returns of ' + symbol, 
        xaxis: {
            title: 'Return'
        },
        yaxis: {
            title: 'Frequency'
        },
        bargap: 0.4
    };

    Plotly.newPlot('graph14_4', data, layout);
}

function plot_spy(stock_data, index) {
    var data = [{
        x: stock_data['date'].slice(index),
        y: stock_data['adj_close'].slice(index),
        type: 'scatter',
        mode: 'lines',
        name: "SPY",
        marker: {
            color: 'rgb(0, 204, 204, 0.8)'
        }
    }];

    var layout = {
        title: "S&P 500 ETF",
        xaxis: {
            title: 'Date'
        },
        yaxis: {
            title: 'Price'
        }
    };

    Plotly.newPlot('graph_spy', data, layout);
}