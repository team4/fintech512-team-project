from flask import (
    Blueprint, Flask, flash, g, redirect, render_template, request, session, url_for, jsonify, make_response
)


from datetime import datetime

from .stock_av import ( 
    get_stock_overview, get_price_series, get_news, get_moving_average
)
from .stock_db import (
    generate_plot_data, ask_buy_order, ask_sell_order, execute_buy_order, execute_sell_order, get_mostly_traded
)
from .auth import login_required

from .ptfl_analysis import calculate_returns, calculate_alpha_beta, calculate_price_diff

from werkzeug.exceptions import abort

bp = Blueprint('stock_info', __name__)

@bp.route('/')
def index():
    
    # SP500 chart
    spy_data = generate_plot_data("SPY", get_price_series("SPY"))
    
    # Trending Stocks
    trending_stocks = get_mostly_traded()
    
    # Recent Headline
    news_data = get_news()     
    return render_template('stock_info/index.html',
                           spy_data=spy_data,
                           trending_stocks=trending_stocks,
                           news_data=news_data)

@bp.route('/info', methods=('GET', 'POST'))
@bp.route('/info/<symbol>')
def info(symbol = None):
    # if we pass symbol as a parameter
    if symbol is not None:
        symbol_ = symbol
        
    # if we past symbol by request    
    elif request.method == 'POST' and 'stockSymbol' in request.form:
        symbol_ = request.form['stockSymbol']
        symbol_ = symbol_.upper()
    else:
        symbol_ = None

    if not symbol_:
        flash('Symbol is required.')
        return redirect(url_for('stock_info.index'))

    else:
        # try:
        stock_prices = get_price_series(symbol_)
        if stock_prices is None:
            error = "Cannot find stock " + symbol_
            flash(error)
            return redirect(url_for('stock_info.index'))   
        
        stock_overview = get_stock_overview(symbol_)
        news = get_news(symbol_)       
        
        plot_data = generate_plot_data(symbol_, stock_prices)
        ma_data = get_moving_average(symbol_)
        

        
        return render_template('stock_info/info.html', 
                                symbol=symbol_,
                    stock_overview=stock_overview, 
                    news_data=news, 
                    plot_data=plot_data,
                    ma_data=ma_data)

        # except Exception as e:
        #     error = "Cannot find stock " + symbol_
        #     flash(error)
    
    # return redirect(url_for('stock_info.index'))
    

@bp.route('/submit_order/<order_type>/<symbol>', methods=('GET', 'POST'))
@login_required
def submit_order(order_type, symbol):
    
    if request.method == 'POST':
        error = None
        quantity = request.form['order_quantity']
        user_id = g.user['id']
        
        if not symbol:
            error = 'Symbol is required.'
            
        if not quantity:
            error = 'Number of shares is required.'
            
        if quantity == 0:
            error = 'Number of shares needs to be greater than 0.'
            
        if error is not None:
            flash(error) 
        else:
            try:
                # convert string to int
                quantity = int(quantity)
                if order_type == "buy":
                    message = ask_buy_order(user_id, symbol, quantity)
                elif order_type == "sell":
                    message = ask_sell_order(user_id, symbol, quantity)
                else:
                    message = {"error": "order type not applicable."}
                
            except Exception as e:
                flash(e)
                return info(symbol)
    
    return render_template('stock_info/order_submission.html', message=message)


@bp.route('/trade/<order_type>/<symbol>', methods=('GET', 'POST'))
@login_required
def execute_order(order_type, symbol):
    
    if request.method == 'POST':
        
        new_balance = request.form['new_balance']
        unit_price = request.form['price']
        quantity = request.form['quantity']
        quantity = int(quantity)
        volume = request.form['volume']
        volume = float(volume)
        user_id = g.user['id']
    
        if order_type == "buy":
            message = execute_buy_order(user_id, new_balance, symbol, unit_price, quantity, volume)
        elif order_type == "sell":
            message = execute_sell_order(user_id, new_balance, symbol, unit_price, quantity, volume)
    
        return render_template('stock_info/order_confirmation.html', message=message)
    
    return redirect(url_for('stock_info.index'))
    
    
# update stock data if necessary
def update_stock_data(symbol):
    stock_prices = get_price_series(symbol)
    generate_plot_data(symbol, stock_prices)
    
    
# populate chart data for the plot
@bp.route('/chart/<symbol>')
def chart(symbol):
    try:
        # stock
        stock_data = generate_plot_data(symbol, get_price_series(symbol))
        stock_prices = {symbol: stock_data['adj_close']}
        stock_returns = calculate_returns(stock_prices)
        stock_diff = calculate_price_diff(stock_prices)
        # SPY
        spy_data = generate_plot_data("SPY", get_price_series("SPY"))
        spy_prices = {"SPY": spy_data['adj_close']}
        spy_returns = calculate_returns(spy_prices)
        spy_diff = calculate_price_diff(spy_prices)
        
        alpha, beta = calculate_alpha_beta(stock_returns[symbol], spy_returns['SPY'], len(stock_returns))
        
    except Exception as e:
        error = "Cannot find stock " + symbol
        flash(error)
        return redirect(url_for('stock_info.index'))
    
    return render_template('stock_info/chart.html', 
                           symbol=symbol, 
                           alpha=alpha,
                           beta=beta,
                           stock_data=stock_data,
                           spy_data=spy_data,
                           stock_returns=list(stock_returns[symbol]),
                           spy_returns=list(spy_returns["SPY"]),
                           stock_diff=list(stock_diff[symbol]),
                           spy_diff=list(spy_diff["SPY"]))