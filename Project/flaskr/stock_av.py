import requests
from datetime import datetime, timedelta

from .config import api_key

# fetch data from Alpha Vantage given the parameter
# return: data (json object)
def get_data(params):
    base_url = "https://www.alphavantage.co/query"
    
    try:
        response = requests.get(base_url, params=params)
        data = response.json()
        return data
    
    except requests.exceptions.RequestException as e:
        print("Error fetching data:", e)
        return None

# return stock overview
# return: result (json object)
def get_stock_overview(symbol):
    
    # define a params json with symbol and API key
    params = {
        "function": "OVERVIEW",
        "symbol": symbol,
        "apikey": api_key
    }
    
    overview = get_data(params)
    if len(overview) > 1:
        result = {
            "Symbol": overview["Symbol"],
            "Exchange": overview["Exchange"],
            "Name": overview["Name"],
            "Industry": overview["Industry"],
            "MarketCapitalization": overview["MarketCapitalization"],
            "PERatio": overview["PERatio"],
            "EPS": overview["EPS"],
            "DividendPerShare": overview["DividendPerShare"],
            "DividendYield": overview["DividendYield"],
            "52WeekHigh": overview["52WeekHigh"],
            "52WeekLow": overview["52WeekLow"]
        }
        return result
    else:
        print("Error fetching stock overview")
        return {}

# return recent 5 year adj closing prices
# return: result (json object)
def get_price_series(symbol):
    
    params = {
        "function": "TIME_SERIES_DAILY_ADJUSTED",
        "symbol": symbol,
        "outputsize": "full",
        "apikey": api_key
    }
    
    prices = get_data(params)
    if len(prices) > 1:
        last_date = prices["Meta Data"]["3. Last Refreshed"]
        date_object = datetime.strptime(last_date, "%Y-%m-%d")
        
        series = prices["Time Series (Daily)"]
        date_length = len(series)
        result = {}
        count = 0
        for i in range(10000):
            # extract 5 years
            if count == 1260 or count == date_length:
                break
            
            date = date_object.strftime("%Y-%m-%d")
            
            # if date does not exist
            while date not in series:
                date_object = date_object - timedelta(days=1)
                date = date_object.strftime("%Y-%m-%d")
                
            # adjusted close price
            data = series[date]  
            
            count = count + 1
            result[i] = {
                "date" : date,
                "price" : data
            }
            # new date for next day
            date_object = date_object - timedelta(days=1)
            
        return result
    else:
        print("Error fetching stock price")
        return None
    
# return 5 recent news feed 
# return: result (json object)
def get_news(symbol=None):
    
    if symbol is None:
        params = {
            "function": "NEWS_SENTIMENT",
            "sort": "LATEST",
            "apikey": api_key
        }
        feed_number = 5
    else:
        params = {
            "function": "NEWS_SENTIMENT",
            "tickers": symbol,
            "sort": "LATEST",
            "apikey": api_key
        }
        feed_number = 5
    
    news = get_data(params)
    if len(news) > 1:
            
        feeds = news["feed"][:feed_number]
        result = {}
        if len(feeds) != 0:
            for i in range(feed_number):
                result[i] = {
                    "id": i,
                    "title": feeds[i]["title"],
                    "url": feeds[i]["url"],
                    "summary": feeds[i]["summary"]
                }
        
        return result
    else:
        print("Error fetching headlines")
        return {}

# return 5 year daily moving average
# return MA10, MA50, MA200
def get_moving_average(symbol):
    
    ma_10 = get_ma_by_period(symbol, 10)
    ma_50 = get_ma_by_period(symbol, 50)
    ma_200 = get_ma_by_period(symbol, 200)
    
    result = {
        'date': ma_10['date'],
        'MA10': ma_10['MA'],
        'MA50': ma_50['MA'],
        'MA200': ma_200['MA']
    }
    return result

# helper function for Moving Average
def get_ma_by_period(symbol, time_period):
    
    params = {
        "function": "SMA",
        "symbol": symbol,
        "interval": "daily",
        "time_period": time_period,
        "series_type": "close",
        "apikey": api_key
    }
    
    ma = get_data(params)
        
    if len(ma) > 1:
        last_date = ma["Meta Data"]["3: Last Refreshed"]
        date_object = datetime.strptime(last_date, "%Y-%m-%d")
        
        series = ma['Technical Analysis: SMA']
        date_length = len(series)
        result = {}
        count = 0
        dates = []
        MAs = []
        for i in range(10000):
            # extract 5 years
            if count == 1260 or count == date_length:
                break
            
            date = date_object.strftime("%Y-%m-%d")
            
            # if date does not exist
            while date not in series:
                date_object = date_object - timedelta(days=1)
                date = date_object.strftime("%Y-%m-%d")
                
            # adjusted close price
            data = series[date]  
            MA_data = data["SMA"]
            
            dates.insert(0, date)
            MAs.insert(0, float(MA_data))
            
            count = count + 1

            # new date for next day
            date_object = date_object - timedelta(days=1)
        
        result = {
            "date": dates,
            "MA": MAs
        }  
        return result
    else:
        print("Error fetching MA", time_period, " for ", symbol)
        return None


# return a single string for the name of the listed company
def get_company_name(symbol):
    
    # define a params json with symbol and API key
    params = {
        "function": "OVERVIEW",
        "symbol": symbol,
        "apikey": api_key
    }
    
    overview = get_data(params)
    if overview is not None:
        return overview["Name"]
    else:
        print("Error fetching company name")
        return None
