from flask import (
    Blueprint, Flask, flash, g, redirect, render_template, request, session, url_for, jsonify, make_response
)

from .auth import login_required
from werkzeug.exceptions import abort

from .user_db import (
    get_user_cash, get_user_orders, get_user_portfolio, get_historical_portfolio, get_watchlist, add_to_watchlist, remove_from_watchlist, get_latest_price_in_ptfl
)

from .ptfl_analysis import analyze_risk
from .stock_info import info

import numpy as np

bp = Blueprint('user_info', __name__)

@bp.route('/watchlist/<user_id>/<type>/<symbol>')
@login_required
def watchlist(user_id, type, symbol):
    if type == "add":
        add_to_watchlist(user_id, symbol)
        message = "Successfully added " + symbol + " to your watchlist."
    else:
        remove_from_watchlist(user_id, symbol)
        message = "Successfully removed " + symbol + " from your watchlist."
        
    flash(message) 
    return info(symbol=symbol)
    
    
@bp.route('/account/<user_id>')
@login_required
def account(user_id):
    
    # total value, stock value, cash
    cash = get_user_cash(user_id)
    historical_data = get_historical_portfolio(user_id)
    if historical_data['message'] == "This user has not traded.":
        holding = 0
    else:
        value = historical_data['value']
        holding = value[-1]
        
    total_holding = cash + holding
    holding_data = {
        "total_holding": round(total_holding, 2),
        "holding": round(holding, 2),
        "cash": round(cash, 2)
    }
    
    # portfolio diversity pie chart
    ptfl_data = get_user_portfolio(user_id)
    symbols = ptfl_data['symbol']   
    if len(symbols) != 0:
        prices = get_latest_price_in_ptfl(symbols)
        quantity = ptfl_data['quantity']
        
        price_array = np.array(prices)
        quantity_array = np.array(quantity)
        product_array = price_array * quantity_array
        diversity_array = product_array / np.sum(product_array)
        diversity = diversity_array.tolist()
        empty_ptfl = False
        
        piechart_data = {
            "symbol": symbols,
            "quantity": quantity,
            "price": prices,
            "diversity": diversity
        }
        
    else:
        empty_ptfl = True
        piechart_data = None
    
    
    # watchlist
    current_watchlist = get_watchlist(user_id)
    
    if current_watchlist['message'] is not None:
        empty_watchlist = True
        watchlist_data = None

    else:
        empty_watchlist = False
        symbols = current_watchlist['symbol']
        prices = current_watchlist['price']
        watchlist_data = {
            "keys": ['Stock', 'Adj. Closed Price'],
            "row_num": len(symbols),
            "symbols": symbols,
            "prices": prices
        }
    
    return render_template('user_info/account.html',
                            holding_data=holding_data,
                            empty_ptfl=empty_ptfl,
                            piechart_data=piechart_data,
                            empty=empty_watchlist,
                            watchlist_data=watchlist_data)

# display user's current portfolio
@bp.route('/investment/<user_id>')
@login_required
def investment(user_id):
    
    # plot data for user holding
    # x: date
    # y: user's portfolio value, weight and price adjusted
    historical_data = get_historical_portfolio(user_id)
    if historical_data['message'] == "This user has not traded.":
        message = "You have not created any portfolio yet."
        return render_template('user_info/investment.html',
                               message=message)
    else:
        date = historical_data['date']
        # convert date to dateString
        date_strings = [d.strftime('%Y-%m-%d') for d in date]
        value = historical_data['value']
        
        ptfl_data = get_user_portfolio(user_id)
        row_num = len(ptfl_data['symbol'])
    
        return render_template('user_info/investment.html',
                                message=None,
                                row_num=row_num, 
                                keys=ptfl_data.keys(),
                                ptfl_data=ptfl_data,
                                plot_date=date_strings,
                                plot_value=value)


# display user's transaction history
@bp.route('/history/<user_id>')
@login_required
def history(user_id):
    order_data = get_user_orders(user_id)
    
    row_num = len(order_data['id'])
    return render_template('user_info/history.html', 
                           row_num=row_num, 
                           keys=order_data.keys(),
                           order_data=order_data)


@bp.route('/analysis/<user_id>')
@login_required
def analysis(user_id):

    ptfl = get_user_portfolio(user_id)
    
    symbols = ptfl['symbol']
    
    # get price data
    
    # analyze risk
    risk_data = analyze_risk(ptfl)
    
    if risk_data == "The portfolio does not contain any asset.":
        return render_template('user_info/analysis.html',
                        message=risk_data)
    else:
    
        past_prices = risk_data["prices"]
        EF_data = risk_data["EF"]
        sharpe_data = risk_data["sharpe"]
        VaR_data = risk_data["VaR"]
        
        return render_template('user_info/analysis.html',
                                message="no error",
                                symbols=symbols,
                                past_prices=past_prices,
                                EF_data=EF_data,
                                sharpe_data=sharpe_data,
                                VaR_data=VaR_data)