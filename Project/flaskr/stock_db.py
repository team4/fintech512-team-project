from datetime import datetime
import pandas_market_calendars as mcal

from .db import get_db

def generate_plot_data(symbol, stock_prices):
    
    db = get_db()
    stock_data = db.execute(
                        'SELECT * FROM stock_data WHERE stock_symbol = ? ORDER BY closing_date DESC', (symbol,)
                    ).fetchall()
                
    date = []
    open = []
    high = []
    low = []
    close = []
    adj_close = []
    volume = []
    
    empty_stock_db = True
    
    if len(stock_data) != 0:
        # keep the stock database current
        # by comparing the last date in DB 
        # versus the last date in AlphaVantage
        recent_date_av = datetime.strptime(stock_prices[0]['date'], "%Y-%m-%d").date()
        recent_date_db = stock_data[0]['closing_date']
        if recent_date_av != recent_date_db:
            db.execute(
                'DELETE FROM stock_data WHERE stock_symbol = ?', (symbol,)
            )
            print("OLD TABLE DELETED DUE TO UNMATCHED DATE")
        else:
            empty_stock_db = False
            
    for i in range(len(stock_prices)):
        daily_prices = stock_prices[i]
        date.insert(0, daily_prices['date'])
        
    if empty_stock_db:
        # if price data for this ticker does not exist
        # insert daily prices into the table
        print("THE STOCK DB IS EMPTY")

        for i in range(len(stock_prices)):
            daily_prices = stock_prices[i]
            db.execute(
                'INSERT INTO stock_data (stock_symbol, closing_date, open_price, high_price, low_price, close_price, adj_close_price, volume)'
                ' VALUES (?, ?, ?, ?, ?, ?, ?, ?)',
                (
                    symbol,
                    daily_prices['date'],
                    daily_prices['price']['1. open'],
                    daily_prices['price']['2. high'],
                    daily_prices['price']['3. low'],
                    daily_prices['price']['4. close'],
                    daily_prices['price']['5. adjusted close'],
                    daily_prices['price']['6. volume']
                )
            )
            
            open.insert(0, daily_prices['price']['1. open'])
            high.insert(0, daily_prices['price']['2. high'])
            low.insert(0, daily_prices['price']['3. low'])
            close.insert(0, daily_prices['price']['4. close'])
            adj_close.insert(0, daily_prices['price']['5. adjusted close'])
            volume.insert(0, daily_prices['price']['6. volume'])

        db.commit()
        print("NEW DB IS CREATED")
    else:
        # if price data already exists
        # read daily prices from the table
        for daily_prices_data in stock_data:
            open.insert(0, daily_prices_data['open_price'])
            high.insert(0, daily_prices_data['high_price'])
            low.insert(0, daily_prices_data['low_price'])
            close.insert(0, daily_prices_data['close_price'])
            adj_close.insert(0, daily_prices_data['adj_close_price'])
            volume.insert(0, daily_prices_data['volume'])
            
        print("FETCH DATA FROM EXISTING DB")

    plot_data = {
        "date": date,
        "open": open,
        "high": high,
        "low": low,
        "close": close,
        "adj_close": adj_close,
        "volume": volume
    }
    return plot_data

# Ask if user wants to execute buy order
def ask_buy_order(user_id, symbol, quantity):
    
    db = get_db()
    order_time = datetime.now().strftime("%Y-%m-%d")
    # check if market is open today
    is_open = market_is_open(order_time)
    if not is_open:
        return {"error": "Market is not open.",
                "symbol": symbol}
    
    unit_price = db_get_price(symbol)
    
    # compare trading volume against user's balance
    volume = unit_price * quantity
    user_balance = db.execute(
        'SELECT balance FROM user WHERE id = ?',
        (user_id,)
    ).fetchone()
    db.commit()
    user_balance = user_balance['balance']
    print("USER BALANCE:", user_balance)

    if volume > user_balance:
        return {"error": "Could not execute trade: Insufficient Fund.",
                "symbol": symbol}
    
    this_holding = db.execute(
        'SELECT * FROM holding WHERE (user_id = ? AND stock_symbol = ?)',
        (user_id, symbol)
    ).fetchall()
    new_balance = user_balance - volume
    
    message = {
        "error": None,
        "type": "buy",
        "symbol": symbol,
        "price": unit_price,
        "quantity": quantity,
        "volume": volume,
        "new_balance": new_balance
    }
    
    return message
    
    
# Ask if user wants to execute sell order
def ask_sell_order(user_id, symbol, quantity):
    
    db = get_db()
    order_time = datetime.now().strftime("%Y-%m-%d")
    # check if market is open today
    is_open = market_is_open(order_time)
    if not is_open:
        return {"error": "Market is not open.",
                "symbol": symbol}
    
    unit_price = db_get_price(symbol)
    
    # compare trading quantity against user's holding
    user_holding = db.execute(
        'SELECT * FROM holding WHERE (user_id = ? AND stock_symbol = ?)',
        (user_id, symbol)
    ).fetchall()
    db.commit()
    
    if len(user_holding) == 0:
        return {"error": "Could not execute trade: Insufficient stock holding.",
                "symbol": symbol}
    else:
        user_quantity = user_holding[0]['quantity']
        print("USER SHARES: ", user_quantity)
        if user_quantity < quantity:
            return {"error": "Could not execute trade: Insufficient stock holding.",
                    "symbol": symbol}
    
    volume = unit_price * quantity
    user_balance = db.execute(
        'SELECT balance FROM user WHERE id = ?',
        (user_id,)
    ).fetchone()
    db.commit()
    user_balance = user_balance['balance']
    print("USER BALANCE:", user_balance)
    new_balance = user_balance + volume    
    
    message = {
        "error": None,
        "type": "sell",
        "symbol": symbol,
        "price": unit_price,
        "quantity": quantity,
        "volume": volume,
        "new_balance": new_balance
    }
    
    return message
    
    
# Execute buy order
def execute_buy_order(user_id, new_balance, symbol, unit_price, quantity, volume):
    return execute_order("buy", user_id, new_balance, symbol, unit_price, quantity, volume)

# Execute sell order
def execute_sell_order(user_id, new_balance, symbol, unit_price, quantity, volume):
    return execute_order("sell", user_id, new_balance, symbol, unit_price, quantity, volume)

# Execute buy or sell order given order type
def execute_order(order_type, user_id, new_balance, symbol, unit_price, quantity, volume):
    order_time = datetime.now().date()
    
    # update user balance 
    db_update_balance(new_balance, user_id)
    
    if order_type == "buy":
        db_update_orders(user_id, "BUY", symbol, order_time, unit_price, quantity, volume)
        
        # update user holding after buying
        holding = db_update_holding(user_id, symbol, unit_price, quantity, volume)
    elif order_type == "sell":
        db_update_orders(user_id, "SELL", symbol, order_time, unit_price, quantity, volume)
        
        # update user holding after selling
        holding = db_update_holding(user_id, symbol, unit_price, -quantity, -volume)
        
    message = {
        "type": order_type,
        "time": order_time,
        "symbol": symbol,
        "order_price": unit_price,
        "order_quantity": quantity,
        "order_volume": volume,
        "average_price": holding["price"],
        "total_quantity": holding["quantity"],
        "total_volume": holding["volume"]
    }
    
    return message

    
# DB helper function
# get latest price for this stock
def db_get_price(symbol):
    db = get_db()
    unit_price = db.execute(
        'SELECT adj_close_price FROM stock_data WHERE stock_symbol = ? ORDER BY closing_date DESC',
        (symbol,),
    ).fetchone()
    db.commit()
    return unit_price['adj_close_price']

# DB helper function
# update user DB user balance
def db_update_balance(balance, user_id):
    db = get_db()
    db.execute(
        'UPDATE user SET balance = ? WHERE id = ?',
        (balance, user_id)
    )
    db.commit()
    print("UPDATED USER BALANCE")
    
# DB helper function
# update orders DB order
def db_update_orders(user_id, order_type, symbol, order_time, unit_price, quantity, volume):
    db = get_db()
    db.execute(
        'INSERT INTO orders (user_id, order_type, stock_symbol, order_time, unit_price, quantity, volume)'
        ' VALUES (?, ?, ?, ?, ?, ?, ?)',
        (user_id, order_type, symbol, order_time, unit_price, quantity, volume)
    )
    db.commit()
    print("UPDATED ORDERS")
    
# DB helper function
# update holding DB user-stock holding
def db_update_holding(user_id, symbol, unit_price, quantity, volume):
    db = get_db()
    this_holding = db.execute(
        'SELECT * FROM holding WHERE (user_id = ? AND stock_symbol = ?)',
        (user_id, symbol)
    ).fetchall()
    db.commit()
    
    holding = {}
    
    if len(this_holding) == 0:
        # after user buy new stock
        db.execute(
            'INSERT INTO holding (user_id, stock_symbol, average_price, quantity, volume)'
            ' VALUES (?, ?, ?, ?, ?)',
            (user_id, symbol, unit_price, quantity, volume)
        )
        db.commit()
        holding = {
            "symbol": symbol,
            "price": unit_price,
            "quantity": quantity,
            "volume": volume
        }
        print("INSERTED INTO HOLDING")
    else:
        
        new_quantity = this_holding[0]['quantity'] + quantity
        if new_quantity == 0:
            # after user sell all shares of this stock
            db.execute(
                'DELETE FROM holding WHERE (user_id = ? AND stock_symbol = ?)',
                (user_id, symbol)
            )
            db.commit()
            print("DELETE THIS HOLDING")
            holding = {
                "symbol": symbol,
                "price": 0,
                "quantity": 0,
                "volume": 0
            }
        else:   
            new_volume = this_holding[0]['volume'] + volume
            new_average_price = new_volume / new_quantity
            
            db.execute(
            'UPDATE holding SET average_price = ?, quantity = ?, volume = ? WHERE (user_id = ? AND stock_symbol = ?)',
                (new_average_price, new_quantity, new_volume, user_id, symbol)
            )
            db.commit()
            print("UPDATED HOLDING")
            holding = {
                "symbol": symbol,
                "price": new_average_price,
                "quantity": new_quantity,
                "volume": new_volume
            }
    
    return holding

# check if market is open today
def market_is_open(date):
    result = mcal.get_calendar("NYSE").schedule(start_date=date, end_date=date)
    return result.empty == False


# find (at most) 10 mostly traded ticker
def get_mostly_traded():
    db = get_db()
    mostly_traded = db.execute(
        'SELECT stock_symbol, SUM(quantity) AS total_quantity FROM orders '
        'GROUP BY stock_symbol ORDER BY total_quantity DESC LIMIT 10'
    ).fetchall()
    
    if len(mostly_traded) != 0:
        symbols = []
        prices = []
        for traded_symbol in mostly_traded:
            symbol = traded_symbol['stock_symbol']
            symbols.append(symbol)
            
            price = db.execute(
                'SELECT adj_close_price FROM stock_data WHERE stock_symbol = ? '
                'ORDER BY closing_date DESC LIMIT 1',
                (symbol,),
            ).fetchone()
            prices.append(price['adj_close_price'])
            
        return {"error": None,
                "symbols": symbols,
                "prices": prices}
    else:
        return {"error": "No trading activity on BigBucks."}