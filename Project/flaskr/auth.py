# BigBucks Project

import functools

from flask import (
    Blueprint, flash, g, redirect, render_template, request, session, url_for
)
from werkzeug.security import check_password_hash, generate_password_hash

from .db import get_db

bp = Blueprint('auth', __name__, url_prefix='/auth')

@bp.route('/register', methods=('GET', 'POST'))
def register():
    if request.method == 'POST':
        username = request.form['username']
        password = request.form['password']
        db = get_db()
        error = None
        
        if not username:
            error = 'Username is required.'
        elif not password:
            error = 'Password is required.'
            
        # protect admin account 
        if username == '__admin__':
            error = "This username is occupied by administrator."
            
        # non-functional requirement: security
        if password == username:
            error = "Password cannot be the same as username."
            
        if error is None:
            try:
                db.execute(
                    "INSERT INTO user (username, password, balance) VALUES (?, ?, ?)",
                    (username, generate_password_hash(password), 1000000),
                )
                db.commit()
                return redirect(url_for("auth.login"))
            except db.IntegrityError:
                error = f"User {username} is already registered. Please log in."
                flash(error)
                return redirect(url_for("auth.login"))

        flash(error)

    return render_template('auth/register.html')


@bp.route('/login', methods=('GET', 'POST'))
def login():
    if request.method == 'POST':
        username = request.form['username']
        password = request.form['password']
        db = get_db()
        error = None
        user = db.execute(
            'SELECT * FROM user WHERE username = ?', (username,)
        ).fetchone()

        if user is None:
            error = 'Incorrect username.'
        elif not check_password_hash(user['password'], password):
            error = 'Incorrect password.'
            
        if error is None:
            session.clear()
            session['user_id'] = user['id']
            # log in to administrator account
            if username == '__admin__':
                return redirect(url_for('admin_info.dashboard'))
            # otherwise go to user's homepage
            else:
                return redirect(url_for('index'))

        flash(error)

    return render_template('auth/login.html')


@bp.route('/reset_password', methods=('GET', 'POST'))
def reset_password():
    if request.method == 'POST':
        username = request.form['username']
        password = request.form['password']
        db = get_db()
        error = None
        user = db.execute(
            'SELECT * FROM user WHERE username = ?', (username,)
        ).fetchone()
        
        # protect admin account 
        if username == '__admin__':
            error = "Cannot reset administrator account."
        
        if password == username:
            error = "Password cannot be the same as username."

        if user is None:
            error = 'Incorrect username.'

        if not password:
            error = 'Password is required.'
            
        print(username)
        print(password)    
        
        if error is None:
            db.execute(
                "UPDATE user SET password = ? WHERE username = ?",
                (generate_password_hash(password), username)
            )
            db.commit()
            return redirect(url_for("auth.login"))
        
        flash(error)
    
    return render_template('auth/reset_password.html')


@bp.before_app_request
def load_logged_in_user():
    user_id = session.get('user_id')

    if user_id is None:
        g.user = None
    else:
        g.user = get_db().execute(
            'SELECT * FROM user WHERE id = ?', (user_id,)
        ).fetchone()
 
       
@bp.route('/logout')
def logout():
    session.clear()
    return redirect(url_for('index'))


def login_required(view):
    @functools.wraps(view)
    def wrapped_view(**kwargs):
        if g.user is None:
            return redirect(url_for('auth.login'))

        return view(**kwargs)

    return wrapped_view