from datetime import datetime

from .db import get_db

def get_orders_by_date(start_date, end_date):
    db = get_db()
    orders = db.execute(
        'SELECT * FROM orders WHERE order_time BETWEEN ? AND ? '
        'ORDER BY order_time DESC',
        (start_date, end_date, ),
    ).fetchall()
    db.commit()
    
    id = []
    user_id = []
    type = []
    symbol = []
    time = []
    price = []
    quantity = []
    volume = []
    
    if len(orders) != 0:
        for order in orders:
            id.append(order['id'])
            user_id.append(order['user_id'])
            type.append(order['order_type'])
            symbol.append(order['stock_symbol'])
            time.append(order['order_time'])
            price.append(order['unit_price'])
            quantity.append(order['quantity'])
            volume.append(order['volume'])
            
    for i in range(len(volume)):
        volume[i] = round(volume[i], 2)
    
    order_data = {
        'id': id,
        'user_id': user_id,
        'type': type,
        'symbol': symbol,
        'time': time,
        'price': price,
        'quantity': quantity,
        'volume': volume
    }
    return order_data


def get_total_portfolio():
    db = get_db()
    total_portfolio = db.execute(
        'SELECT stock_symbol, SUM(quantity) as total_quantity FROM holding GROUP BY stock_symbol'
    ).fetchall()
    db.commit()
    
    symbol = []
    quantity = []
    
    if len(total_portfolio) != 0:
        for stock_holding in total_portfolio:
            symbol.append(stock_holding['stock_symbol'])
            quantity.append(stock_holding['total_quantity'])
            
    portfolio_data = {
        'symbol': symbol,
        'quantity': quantity
    }
    
    return portfolio_data


def get_all_user_portfolio():
    db = get_db()
    user_portfolio = db.execute(
        'SELECT u.id as user_id, u.username, h.stock_symbol, h.average_price, h.quantity '
        'FROM holding h INNER JOIN user u ON h.user_id = u.id ORDER BY u.id'
    ).fetchall()
    db.commit()
    
    user_id = []
    username = []
    symbol = []
    average_cost = []
    quantity = []
         
    
    if len(user_portfolio) != 0:
        for asset in user_portfolio:
            user_id.append(asset['user_id'])
            username.append(asset['username'])
            symbol.append(asset['stock_symbol'])
            average_cost.append(asset['average_price'])
            quantity.append(asset['quantity'])
            
    for i in range(len(average_cost)):
        average_cost[i] = round(average_cost[i], 2)
            
    portfolio_data = {
        'user_id': user_id,
        'username': username,
        'symbol': symbol,
        'average_cost': average_cost,
        'quantity': quantity,
    }
    
    return portfolio_data