import os

from flask import Flask

def create_app(test_config=None):
    # create and configure the app
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_mapping(
        SECRET_KEY='dev',
        DATABASE=os.path.join(app.instance_path, 'flaskr.sqlite'),
    )
    
    
    if test_config is None:
        # load the instance config, if it exists, when not testing
        app.config.from_pyfile('config.py', silent=True)
    else:
        # load the test config if passed in
        app.config.from_mapping(test_config)
    
    # ensure the instance folder exists
    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass
    
    # register BP for authentication and login
    from . import auth
    app.register_blueprint(auth.bp)
    
    # register BP for administrator 
    from . import admin_info
    app.register_blueprint(admin_info.bp)
    
    # register BP for user account info
    from . import user_info
    app.register_blueprint(user_info.bp)
    
    # register BP for stock info
    from . import stock_info
    app.register_blueprint(stock_info.bp)
    app.add_url_rule('/', endpoint='index')
    
    # register BP for stock database
    from . import db
    db.init_app(app)
            
    return app
    
     
    