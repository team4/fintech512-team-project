INSERT INTO user (username, password, balance)
VALUES
  ('test', 'pbkdf2:sha256:50000$TCI4GzcX$0de171a4f4dac32e3364c7ddc7c14f3e2fa61f2d17574483f7ffbb431b4acb2f', 1000000),
  ('other', 'pbkdf2:sha256:50000$kJPKsz6N$d2d4784f1b030a9761f5ccaeeaca413f27f2ecb76d6168407af962ddce849f79', 1000000);

INSERT INTO stock_data (stock_symbol, closing_date, open_price, high_price, low_price, close_price, adj_close_price, volume)
VALUES
    ('AAPL', '2024-03-28', 171.75, 172.23, 170.51, 171.48, 171.48, 65672700)