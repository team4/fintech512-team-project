## Sprint 4 Plan

Team 4

### 1. Continue Server Initialization and Upgrades

Our objectives are to:

1. Ensure continued access for all team members to the VM server.
2. Guarantee the functionality of the BigBucks website on the server with any new updates.

### 2.  Enhance the Code Repository

1. Further develop and organize the project code repository.
2. Ensure proper documentation and version control practices are being followed.

### 3. Front-end Development

1. Develop and design front-end pages that better reflect the user experience and interface based on the finalized requirements.

### 4. Back-end Development and Testing

1. Implement new back-end functionalities as per the updated design diagrams.
2. Write and update unit tests for the newly developed components to ensure stability and performance.
3. Implement and test the following functionalities following project rubric:
* R4: Develop the Sharpe Ratio calculation for the user's portfolio.
* R4: Implement the Efficient Frontier for portfolio analysis.
* R3: Create functionality to track a 5-year history for each user.
* R3: Integrate tracking of the 5-year SPY (S&P 500 ETF) history.

### 5. Task Tracking and GitLab Issue Management

1. Maintain a clear list of open and closed GitLab issues.
2. Use this list to track our progress and address any blockers promptly.
