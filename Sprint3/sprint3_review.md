## Sprint 3 Review

Team 4

### 1. Meeting with TA

Our team had a meeting with TA, during which we reported our weekly progress, showed our progress, and finished the buy and sell trasactions parts.

### 2. User login

Our team added features for the customer account. Now a typical customer can register an account, login, and reset password if they forgot it. 

### 3. User login test case

We finished testing on the first requirement - user registration / login process, and passed the pytest. 


### 4. Create user story for buy/sell

We create user story implementing market orders for efficient stock trading including market order execution, historical simulation, and initial funding, etc.

### 5. Enable buy/sell feature

After the users logged in their accounts, they can search the stock and buy or sell that equity. Corresponding changes will be made to their account balance and holdings. That order will also be added to the transaction history database.

### 6. Buy/sell test case

Our team also wrote two test cases for the buy/sell stocks. We have not implemented code yet. The test case for this part will be finalized by sprint 4. 

### 6. GitLab Issue

Our team got used to the GitLab Issue page, and this week's backlogs were written in the form of GitLab Issue with the tag **Product Backlog**.

Besides, we also created project related issues tagged differently. The headline always  begins with "Sprint *". 
If the task is related to specific requirements in the grading template, we will add "R*" after "Sprint *". 

