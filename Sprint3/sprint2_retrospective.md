## Sprint 2 Retrospective Review

Team 4

### 1. Meeting with TA
This past week, our team successfully conducted a meeting with our TA, Rebecca Xiao (qx42). Our meetings are now set for 6 PM every Friday, which accommodates all team members' schedules better. The TA provided valuable feedback on our progress and addressed some of the queries we had.

### 2. Update UML Diagram
Based on the revised requirements table, which we refined after receiving input from our instructors and TAs, we updated our UML class diagram. This update was inspired by the Project Grading Template, ensuring our project aligns with the expected academic standards and project requirements. 

### 3. Build Server
Our team made significant progress in terms of infrastructure setup by reserving and setting up a Virtual Machine (VM) on the Duke Server website. This VM will serve as our build server, facilitating continuous integration and deployment for our project. The setup process was completed successfully, and we plan to manage this setting actively to align with our development needs, ensuring that our server is operational when necessary.

### 4. GitLab Issue Management
Throughout this sprint, the team has grown more accustomed to utilizing GitLab Issues for tracking and managing our project tasks. This week, we transitioned our product backlog into GitLab Issues, tagging each entry with 'Product Backlog' for easier identification and prioritization. This method has enhanced our task management process, allowing for more transparent and efficient workflow coordination among team members.

### 5. Code Testing
In alignment with our project roadmap, we conducted tests on our current version of the BigBucks page to ensure our project meets the desired functionality and quality standards.
