## Sprint 6 Plan

Team 4

Our team are going to finish all the requirements listed on the project description. We are reviewing our progress and try to figure out what is missing. 

### 1. GitLab Issues
Our team will create GitLab Issues to document the progress. 

### 2. Functional Requirement: Charts
Our team will finish the charts as a part of functional requirements. Then we will further optimize our "watchlist" feature. 

### 3. Non-functional Requirement: CSS style
Our team will complete the stylesheet for the front-end pages

### 4. VCM server
After our team have finished a deliverable product and tested it locally, we will pack the package to the VCM server.

### 5. Test cases
We will write more test cases, and implement some of the test cases to the back-end to test the robustness and features for the database.

### 6. Code Review: Part 6
After our team finishes all functional requirements, we will complete the code review for those parts.

### 7. Demonstration of UI/UX principle
We are going to demonstrate the UX principles (Gestalt principle) and explain how we put them in our user interface.

### 8. Project Topic 1: C4 Diagram
We are going to write a class diagram based on C4 architecture for better abstraction.

### 9. Project Topic 2: Document Generative AI usage
We have extensively used ChatGPT as a tool to help developing. We are going to document how we use that.

### 10. Project Video
We will record a video to demonstrate the BigBucks project to customers. 
