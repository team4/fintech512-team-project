## FinTech 512 Team Project

Team 4 

## 1. READ before use

1. Remember to git **pull** from remote whenever opening the repository on your local device. Otherwise the branch will diverge. 

## 2. Virtual Machine

1. On MacOS, use Terminal to access

    Login: ssh **netid**@team4.colab.duke.edu

    e.g. 
    > ssh hx93@team4.colab.duke.edu

    Enter your own Duke NetID password

2. Administrative access

    type
    > sudo su -
    
    Then enter your own Duke NetID password

3. Website address: http://team4.colab.duke.edu:5000/


## 3. Flask Environment

After run 
> flask --app flaskr run --debug

If this shows up in terminal
> Usage: flask [OPTIONS] COMMAND [ARGS]...

> Try 'flask --help' for help.

> Error: No such option: --app

Try 
> pip install --upgrade Flask

> pip install --upgrade watchdog

> pip install "pyqt5<5.13"

Then the command should work on local machine. 

Type "pip install -r requirements.txt" to install dependencies


## 4. Log in option

Normal user: 
> username: team4

> password: fintech512


Administrator:
> username: \_\_admin\_\_

> password: bigbucks


## 5. Flask Stock Project

1. The Alpha Vantage API key is stored in local config.py, which could be accessed by my stock_info.py

2. To start the web server, change directory to stock_info, run "flask --app flaskr run --debug" in terminal

3. To access a particular stock, type the correct stock symbol, such as TSLA for Tesla, then the relevant information will pop on the website. 

4. User login is required to view stock data. Click "Register" to register, and "Log In" to log in. Click "Log out" to log out. Username is "team4", password is "fintech512". 

5. After modifying schema.db, run "flask --app flaskr init-db" in terminal to initialize the database. Expected to see "Initialized the database."


## 6. Add API key

1. The GitLab repository does not contain API key. To add a local API key, add a config.py inside the Project/flaskr/ folder, adn type "api_key = "XXXX" " where XXXX is your API key.

