## Sprint 5 Plan

Team 4

Next week we will have final meeting with TA, therefore we strictly align our plan with the grading template. This plan's scope is beyond sprint 5. Our team will reference this guide until submission. 

To edit this file, click Edit -> Edit single file, and replace the blank space with "x": - [ ] -> - [x]. After you finish editing, commit changes. 

### 0. GitLab Issue

- [x] For each issue listed on GitLab, a description is necessary.

- [ ] Add a time estimation on each issue. 

### 1. Enable Customers

- [ ] User story

- [ ] VCM server

- [ ] Code Review

### 2. Buy/Sell Shares

- [x] User story is already written. We need to move the user story to the single folder 

- [x] Test cases is already written. We want to add a few cases, and write code for some of them.

- [ ] Code Review

### 3. Stock History

- [ ] User story

- [ ] Test cases/results

- [ ] Code Review

- [x] 5-year historical data for user's portfolio

- [ ] 5-year SPY

- [x] keep data current

### 4. Risk Return Analysis

- [ ] User story

- [ ] Test cases/results

- [ ] Code Review

- [x] fork/branch

- [x] Admin access

### 5. Reports

- [ ] User story

- [ ] Test cases/results

- [ ] Code Review

- [x] fork/branch

- [x] User can list shares owned

- [x] Admin: list all stocks

- [x] Admin: list orders for a certain date. We want to set a range instead of a single date. 

### 6. Advanced Charting

- [ ] User story

- [ ] Test cases/results

- [ ] Code Review

- [ ] fork/branch

- [ ] Figure 14.1 adj close price

- [ ] Figure 14.2 daily simply return

- [ ] Figure 14.3 daily return vs previous daily

- [ ] Figure 14.4 Histogram of daily returns

- [ ] Figure 7.1 Stock price vs index

- [ ] Figure 7.2 daily simply return vs index over time

- [ ] Figure 7.3 scatter of stock vs index

### 7. UI/UX Paper Prototype

- [ ] User story: The user story of someone who wants to see a paper prototype. e.g. A UI/UX designer. 

- [ ] Paper prototype

### 8. Balsamiq Wireframe Prototype

- [ ] User story: The user story of someone who wants to see a wireframe prototype.

- [ ] UX principles applied in discussion: Write a discussion file to discuss about the UX principles. 

- [ ] Wireframes

### 9. Wireframe Implementation

- [ ] User story: The user story of someone who wants to see a wireframe implementation.

- [ ] Test cases/results

- [ ] Code review

- [ ] fork/branch

- [ ] implementation

- [ ] Demonstration of UX principles