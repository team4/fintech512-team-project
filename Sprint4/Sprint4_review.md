## Sprint 4 Review

Team 4

### 1. Meeting with TA 
Our team had a productive meeting with the TA, during which we reported our weekly progress. We demonstrated our code directly through VS Code, showcasing features from the login page to the functionalities for buying/selling stocks and managing assets. Next week, we plan to change our presentation format to have each team member report their progress individually, rather than having a single spokesperson.

### 2. Completion of Buy/Sell Transaction Code
We successfully completed the coding for the buy/sell transactions. User can see the transaction interface after they search for the stock.

### 3. Addition of Test Cases 
We have added test cases to enhance the reliability and performance of our application. We will upload written test cases to the tests folder, and codes to Project/tests folder

### 4. Plan to move all user story to another single folder
We are planning to move all user stories to a single folder to streamline our documentation and workflow processes. This also applies to "code review" part.