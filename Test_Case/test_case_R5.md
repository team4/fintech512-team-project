## Test Case 1: User Report
Objective: Verify that the Investment Reports page accurately reflects current portfolio holdings, average cost, and quantities.
Input: User navigates to the Investment Reports section to generate a report of current holdings.
Expected Results:
Upon accessing the Investment Reports page, the system should display a line chart and list of all currently held stocks: (eg. Symbol: MSFT, Average Cost: $421.9, Quantity: 150 shares).
The list should include the correct ticker symbol, average cost, and quantity for each stock based on the user’s transaction history.
The average cost per share should be calculated based on the aggregated cost of all purchases for each stock.

## Test Case 2: Administrator’s Market Order and Portfolio Summary
Objective: Confirm that the administrator’s views correctly summarize the market orders and portfolio holdings for all users on a specific date.
Input: Administrator sets the date to 2024-04-22 and selects to view market orders and user portfolios.
Expected Results: For each transaction, the table must show the order ID, user ID, transaction type (BUY/SELL), stock symbol, time of transaction, price per share, quantity, and total volume. The information for each transaction should match the recorded market orders, e.g., Transaction ID 22 should show a BUY order for user ID 4 for AAPL with the time stamped on 2024-04-22, price per share at $165, quantity of 100 shares, and a total volume of $16,500.

## Test Case 3: Administrator’s Report on User Portfolio
Objective: Confirm that the administrator’s views correctly display all users' portfolios.
Input: Administrator select a User ID of ada and team 4.
Expected Results: When the administrator views the 'All Users', the system should display a table with user_id, username, stock symbol, average cost, and quantity. For user ID 2, under username 'team4', the table should list all stocks held such as IBM, DIS, SBUX, MUFG, GS, and PDD with their respective average costs and quantities. All average costs should be calculated correctly based on the cumulative cost of all purchases for each stock divided by the total shares held.



