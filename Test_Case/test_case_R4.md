## Test Case 1: Risk Return Analysis - Sharpe ratio
Input: Buy more 100 shares of MSFT on 2024-04-12
Expected Output: The portfolio's position moves slightly to the right on the efficient frontier graph, indicating a slight increase in risk due to the additional investment in MSFT shares. The Sharpe ratio is recalculated is 0.05221801990884291, reflecting the new expected return (0.1101) and standard deviation (1.7655) of the updated portfolio.

## Test Case 2: Risk Return Analysis - VaR 
Input: Buy more 100 shares of MSFT on 2024-04-12
Expected Output: The result shows that the 95% Value at Risk (absolute) is 0.028 and the 95% Value at Risk (difference) is 0.029.