## Test Case 1: Stock History
Objective: Ensure that the stock history page correctly displays the transaction history for a user's account.
Input: User accesses the account history page to review past transactions.
Transaction ID 15 - Type: BUY, Stock Symbol: MSFT, Date & Time: 2024-04-12, Price per Share: $421.9, Quantity: 100 shares, Total Volume: $42,190.00.
Transaction ID 16 - Type: SELL, Stock Symbol: MSFT, Date & Time: 2024-04-12, Price per Share: $421.9, Quantity: 50 shares, Total Volume: $21,095.00.
Transaction ID 17 - Type: BUY, Stock Symbol: AAPL, Date & Time: 2024-04-12, Price per Share: $176.55, Quantity: 100 shares, Total Volume: $17,655.00.
Transaction ID 18 - Type: BUY, Stock Symbol: DIS, Date & Time: 2024-04-12, Price per Share: $114.07, Quantity: 100 shares, Total Volume: $11,407.00.
Transaction ID 19 - Type: BUY, Stock Symbol: AMC, Date & Time: 2024-04-12, Price per Share: $2.645, Quantity: 100 shares, Total Volume: $264.50.
