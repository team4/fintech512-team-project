## Test Case 1: Enable Customers - create account
Input: Navigate to the 'Register' page, input Username: 'team4', input Password: fintech512.
Expected Output: 'User team4 is already registered. Please log in.'

## Test Case 2: Enable Customers - Same username and password
Input: Navigate to the 'Register' page, input Username: 'ada', input Password: ada.
Expected Output: 'Password cannot be the same as username.'

## Test Case 3: Enable Customers - Existing Username
Input: Navigate to the 'Register' page, input Username: team4(assuming team4 already exists), input Password: fintech512.
Expected Output: 'User team4 is already registered. Please log in.' User is prompted to choose a different username.

## Test Case 4: Enable Customers - Log in correctly
Input: Navigate to the 'Log In' page, input Username: 'team4', input Password: 'fintech512'.
Expected Output: User is successfully logged in and redirected to the homepage.

## Test Case 5: Enable Customers - Log in with incorrect password or non-existent username
Input: Navigate to the 'Log In' page, input Username: 'team4', input Password: 'fintech512'.
Expected Output: 'Incorrect password.' User could input again or click forget password.

## Test Case 6: Enable Customers - Forget password
Input: Navigate to the 'Log In' page and click on the 'Forget Password' link.
Expected Output: 'RESET PASSWORD'.

## Test Case 7: Enable Customers - Stock Search
Input: User types "AAPL" into the search bar and submits the query.
Expected Output: The system should display the stock information page for Apple Inc. (AAPL) with current price, historical price chart, moving averages, and other relevant data.

## Test Case 8: Enable Customers - Watchlist Addition & Removal
Input: User clicks 'Add to Watchlist' and then 'Remove from Watchlist' for AAPL stock.
Expected Output: AAPL should be added to the user's watchlist and then removed when the respective button is clicked, with the system providing visual confirmation of the action.

## Test Case 9: Enable Customers - News Headlines Display
Input: User types "AAPL" into the search bar and submits the query.
Expected Result: The headlines should be up-to-date and pertinent to Apple Inc., aiding the user in making informed trading decisions.

## Test Case 10: Administrator Login Functionality
Input: Administrator enters their credentials on the login page.
Expected Results: The administrator should be able to log in using their designated username and password. They could see overall risk, all users and market orders on the website.