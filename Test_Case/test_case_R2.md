## Test Case 1: Buy Transaction
Input: Buy 100 shares of MSFT on 4/12/2024.
Expected Output: Debit cash $42,190.00, credit MSFT 100 shares. The exact price per share would be determined based on the Adjusted Close price of MSFT on 1/20/2021.

## Test Case 2: Sell Transaction
Input: Sell 50 shares of MSFT on 4/12/2024.
Expected Output: Debit MSFT 50 shares, credit cash $21,095.00. The price per share is based on the Adjusted Close price of MSFT on 4/12/2024.

## Test Case 3: Sell more shares than are owned
Input: Sell 200 shares of AMC while only owning 100 shares of AMC.
Expected Output: The system should prevent the transaction and shows 'Could not execute trade: Insufficient stock holding.'

## Test Case 4: Buy shares with insufficient fund
Input: Buy 100000000 shares of AAPL while only owning $100,0000 for individual cash.
Expected Output: The system should prevent the transaction and shows 'Could not execute trade: Insufficient Fund.'

## Test Case 5: Buy or sell shares on non-trading day
Input: Buy 100 shares of AAPL on non-trading day (eg. Saturday)
Expected Output: The system should prevent the transaction and shows 'Market is not open.'

## Test Case 6: Buy invalid stocks
Input: Buy 100 shares of AAAAAA
Expected Output: The system should prevent the transaction and shows 'Cannot find stock AAAAAA'.
