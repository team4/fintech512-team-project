## Test Case 1: Advanced Analytics and Charting
Objective: Ensure that the advanced analytics and charting tools function correctly, providing accurate and detailed visual data representation.
Input: User selects a stock (AAPL) and navigates through various advanced charting options.
Expected Results:
1. **Price Plot Test:**
   - The price plot for AAPL should match historical data and present an accurate graph of adjusted closing prices over the past year.
   
2. **Returns Plot Test:**
   - The daily returns, autocorrelated returns, and frequency distribution plots for AAPL should accurately represent the stock's historical returns data.

3. **Comparative Index Test:**
   - The 'Price vs. Index' and 'Return vs. Index' charts should correctly overlay the selected stock price and return data against the chosen market index (S&P 500) for the same time period.

4. **Scatter Plot Correlation Test:**
   - The scatter plot should correctly plot the daily returns of AAPL against the S&P 500 index returns, displaying a regression line that indicates the correlation between them.

