## Calculate the SLOC
| Language | Files | %     | Code | %   | Comment | %  |
|----------|-------|-------|------|-----|---------|----|
| Python   | 2     | 100.0 | 305  | 68.1| 32      | 7.1|
| Sum | 2     | 100.0 | 305  | 68.1| 32      | 7.1|

## Overview
This document provides a code review of the `user_info` and `user_db` modules in a Flask application. These modules are responsible for managing user interactions with their portfolio, including viewing and editing watchlists, viewing account details, and accessing historical trading data.

## SOLID Principles Review
### Single Responsibility Principle (SRP)
- **Issue:** Functions such as `account`, `investment`, and `history` within the `user_info` module handle both the logic for data retrieval and presentation logic. This mixing of concerns complicates testing and maintenance.
- **Recommendation:** Separate these concerns by moving data retrieval and business logic to the `user_db` module, keeping `user_info` focused on presentation and user interaction.

### Open/Closed Principle (OCP)
- **Issue:** The `user_db` functions are tightly coupled with specific SQL queries and database schema, making it hard to extend or modify the application without modifying existing code.
- **Recommendation:** Define interfaces for database operations, allowing for different implementations that can be easily swapped out without affecting the rest of the application.

### Interface Segregation Principle (ISP)
- **Issue:** The `user_db` module's functions are accessed by various parts of the application, which may not require all the functionality provided by these functions.
- **Recommendation:** Break down the `user_db` module into smaller, more focused interfaces. For example, separate interfaces for handling watchlists, transactions, and user portfolio data can be created.

### Dependency Inversion Principle (DIP)
- **Issue:** High-level modules (`user_info` handlers) are directly dependent on low-level modules (`user_db` SQL queries).
- **Recommendation:** Abstract the database access layer and use dependency injection to provide `user_info` with the needed data access objects. This would decouple the high-level modules from the low-level database access logic.
