## Calculate the SLOC
| Language | Files |     % | Code |    % | Comment |   % |
|----------|-------|-------|------|------|---------|-----|
| Python   |     1 | 100.0 |  103 | 68.7 |       6 | 4.0 |
| Sum  |     1 | 100.0 |  103 | 68.7 |       6 | 4.0 |

## Single Responsibility Principle (SRP)

- **Issue**: Blueprints such as `auth` and `user_info` handle multiple responsibilities including database interactions, user authentication, and business logic.
- **Recommendation**: Separate these concerns by creating dedicated services for tasks like user validation, password management, and database operations to improve modularity and maintainability.

## Interface Segregation Principle (ISP)

- **Issue**: Modules might depend on interfaces that include methods they do not use.
- **Recommendation**: Design modules and interfaces to ensure that components are only dependent on the methods they use, which simplifies dependencies and improves code clarity.

## Dependency Inversion Principle (DIP)

- **Issue**: High-level modules depend directly on low-level modules (e.g., direct database calls within routes).
- **Recommendation**: Implement an abstraction layer for database access and interaction, allowing high-level modules to depend on abstractions rather than concrete implementations. This can improve flexibility and decouple components.
